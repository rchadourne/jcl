//JOB01    JOB (),'MERGE PS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* MERGE TWO PS IN A ANOTHER PS
//* **************************************************************
//STEP01   EXEC PGM=SORT
//STEPLIB  DD   DSN=SYS1.SORTLIB,DISP=SHR
//SORTIN01 DD   DISP=SHR,DSN=dsname-in1
//SORTIN02 DD   DISP=SHR,DSN=dsname-in2
//SORTWK01 DD   UNIT=SYSDA,SPACE=(CYL,(1,1))
//SORTOUT  DD   DSN=dsname-out,DISP=(,CATLG,DELETE),
//         DCB=(RECFM=FB,LRECL=80),UNIT=SYSDA,
//         SPACE=(TRK,(5,2),RLSE)
//SYSOUT   DD   SYSOUT=*
//SYSIN    DD   *
  MERGE FIELDS=(1,10,CH,A)
/*

